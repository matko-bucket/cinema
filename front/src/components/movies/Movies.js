import React from 'react';
import {Button, Table, Form, Col, Row} from 'react-bootstrap'
import CinemaAxios from './../../apis/CinemaAxios';

class Movies extends React.Component {

    constructor(props) {
        super(props);

        const search = {
            movieName: "",
            durationMin: "",
            durationMax: "",
            genreId: -1,
          }

        this.state = { 
            movies: [],
            genres: [],
            pageNo: 0,
            totalPages: 1,
            search: search
        }
    }

    componentDidMount() {
        this.getMovies(0);
        this.getGenres();
    }

    getMovies(newPageNo) {
        const config = {
            params: {
              pageNo: newPageNo
            }
          }
      
          if(this.state.search.movieName!=""){
            config.params['naziv'] = this.state.search.movieName
          }
          if(this.state.search.durationMin!=""){
            config.params['trajanjeOd'] = this.state.search.durationMin
          }
          if(this.state.search.durationMax!=""){
            config.params['trajanjeDo'] = this.state.search.durationMax
          }
          if(this.state.search.genreId!=-1){
            config.params['zanrId'] = this.state.search.genreId
          }
         

        CinemaAxios.get('/filmovi', config)
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({movies: res.data});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    getGenres(){
        CinemaAxios.get('/zanrovi')
        .then(res => {
             // handle success
             console.log(res);
             this.setState({genres: res.data});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
        });
    }

    getGenresStringFromList(map) {
        return Object.values(map).join(", ");
    }

    goToEdit(movieId) {
        this.props.history.push('/movies/edit/'+ movieId); 
    }

    deleteFromState(movieId) {
        var movies = this.state.movies;
        movies.forEach((element, index) => {
            if (element.id === movieId) {
                movies.splice(index, 1);
                this.setState({movies: movies});
            }
        });
    }

    delete(movieId) {
        CinemaAxios.delete('/filmovi/' + movieId)
        .then(res => {
            // handle success
            console.log(res);
            alert('Movie was deleted successfully!');
            this.deleteFromState(movieId); // ili refresh page-a window.location.reload();
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    goToAdd() {
        this.props.history.push('/movies/add');  
    }

    searchValueInputChange(event){
        const name = event.target.name;
        const value = event.target.value;
    
        let search = this.state.search;
        search[name] = value
        this.setState(search)
      }


    renderMovies() {
        return this.state.movies.map((movie, index) => {
            return (
               <tr key={movie.id}>
                  <td>{movie.naziv}</td>
                  <td>{movie.trajanje}</td>
                  <td>{this.getGenresStringFromList(movie.zanrovi)}</td>
                  {window.localStorage['role']=="ROLE_ADMIN"?
                  [
                    <td><Button variant="warning" onClick={() => this.goToEdit(movie.id)}>Edit</Button></td>,
                    <td><Button variant="danger" onClick={() => this.delete(movie.id)}>Delete</Button></td>
                  ]
                  :null}
               </tr>
            )
         })
    }

    render() {
        return (
            <div>
                <h1>Movies</h1>
                
                <Form style={{marginTop:35}}>

                    
                   
                    <Form.Group>
                    <Form.Label>Movie name</Form.Label>
                    <Form.Control
                    value={this.state.search.movieName}
                    name="movieName"
                    as="input"
                    type="text"
                    onChange={(e) => this.searchValueInputChange(e)}
                    ></Form.Control>
                    </Form.Group>

                    <Row>
                    <Col md={6}>
                        <Form.Group>
                        <Form.Label>Duration from</Form.Label>
                        <Form.Control
                        value={this.state.search.duration}
                        name="durationMin"
                        as="input"
                        type="number"
                        onChange={(e) => this.searchValueInputChange(e)}
                        ></Form.Control>
                    </Form.Group>
                    </Col>

                    <Col md={6}>
                        <Form.Group>
                        <Form.Label>Duration until</Form.Label>
                        <Form.Control
                        value={this.state.search.duration}
                        name="durationMax"
                        as="input"
                        type="number"
                        onChange={(e) => this.searchValueInputChange(e)}
                        ></Form.Control>
                    </Form.Group>
                    </Col>
                    </Row>
                    
                    <Form.Group>
                        <Form.Label>Genres</Form.Label>
                        <Form.Control
                        onChange={(event) => this.searchValueInputChange(event)}
                        name="genreId"
                        value={this.state.search.genreId}
                        as="select">
                        <option value={-1}></option>
                        {this.state.genres.map((genre) => {
                            return (
                            <option value={genre.id} key={genre.id}>
                                {genre.naziv}
                            </option>
                            );
                        })}
                        </Form.Control>
                    </Form.Group>
                    <Button onClick={() => this.getMovies(0)}>Search</Button>
                </Form>
                <br></br>
                <div>
                    {window.localStorage['role']=="ROLE_ADMIN"?
                    <Button onClick={() => this.goToAdd() }>Add</Button>
                    :null}
                    <br/>
                    
                    <Table striped style={{marginTop:5}}>
                        <thead className="table-dark" >
                            <tr>
                                <th>Name</th>
                                <th>Duration (min)</th>
                                <th>Genres</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderMovies()}
                        </tbody>                  
                    </Table>
                </div>
            </div>
        );
    }
}

export default Movies;