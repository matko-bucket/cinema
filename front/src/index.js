import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, HashRouter as Router, Switch, Redirect } from "react-router-dom";
import { Navbar, Nav, Container, Button } from "react-bootstrap";
import Home from "./components/Home";
import AddMovie from "./components/movies/AddMovie";
import EditMovie from "./components/movies/EditMovie";
import Movies from "./components/movies/Movies";
import Projections from './components/projections/Projections';
import AddProjection from './components/projections/AddProjection';
import Login from './components/authorization/Login';
import NotFound from "./components/NotFound";
import {logout} from './services/auth';
import Registration from "./components/registration/Registration";


class App extends React.Component {
  render() {

    const jwt = window.localStorage['jwt'];

    if(jwt){return (
      <div>
        <Router>
          <Navbar expand bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/">
                Home
            </Navbar.Brand>
            <Nav>
              <Nav.Link as={Link} to="/movies">
                Movies
              </Nav.Link>
              <Nav.Link as={Link} to="/projections">
                Projections
              </Nav.Link>
                <Button onClick={()=>logout()}>Logout</Button>
            </Nav>
          </Navbar>
          <Container style={{paddingTop:"25px"}}>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login"  render={()=><Redirect to="/movies"/>}/>
              <Route exact path="/movies" component={Movies} />
              <Route exact path="/movies/add" component={AddMovie} />
              <Route exact path="/movies/edit/:id" component={EditMovie} />
              <Route exact path="/projections" component={Projections} />
              <Route exact path="/projections/add" component={AddProjection} />
              <Route component={NotFound} />
            </Switch>
          </Container>
        </Router>
      </div>
    );
  }else{
    return( 
      <Router>
          <Navbar expand bg="dark" variant="dark">
              <Navbar.Brand as={Link} to="/">
                  Home
              </Navbar.Brand>
              <Nav>
                <Nav.Link as={Link} to="/movies">
                Movies
                </Nav.Link>
                <Nav.Link as={Link} to="/projections">
                Projections
                </Nav.Link>
                <Nav.Link as={Link} to="/registration">
                Registration
                </Nav.Link>
                <Nav.Link as={Link} to="/login">
                Login
                </Nav.Link>
              </Nav>
          </Navbar>
          <Container>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Login}/>
              <Route exact path="/movies" component={Movies} />
              <Route exact path="/projections" component={Projections} />
              <Route exact path="/registration" component={Registration} />
            </Switch>
          </Container>
      </Router>);
   
  }
      
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
